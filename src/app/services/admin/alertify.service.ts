import { Injectable } from '@angular/core';
declare var alertify: any;

@Injectable({
  providedIn: 'root',
})
export class AlertifyService {
  constructor() {}

  message(message: string, options: Partial<AlertifyOptions>) {
    alertify.set('notifier', 'delay', options.delay);
    alertify.set('notifier', 'position', options.position);
    const msg =
      alertify[options.messageType ?? AlertifyMessageType.Message](message);

    if (options.dismissOthers) {
      msg.dismissOthers();
    }
  }

  dismiss() {
    alertify.dismiss();
  }

  dismissAll() {
    alertify.dismissAll();
  }
}

export class AlertifyOptions {
  messageType: AlertifyMessageType = AlertifyMessageType.Message;
  position: AlertifyPosition = AlertifyPosition.BottomRight;
  delay: number = 5;
  dismissOthers: boolean = false;
}

export enum AlertifyMessageType {
  Error = 'error',
  Warning = 'warning',
  Success = 'success',
  Notify = 'notify',
  Message = 'message',
}

export enum AlertifyPosition {
  TopLeft = 'top-left',
  TopCenter = 'top-center',
  TopRight = 'top-right',
  BottomLeft = 'bottom-left',
  BottomCenter = 'bottom-center',
  BottomRight = 'bottom-right',
}
