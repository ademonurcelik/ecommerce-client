import { Injectable } from '@angular/core';
import { HttpClientService } from '../http-client.service';
import { Create_Product } from 'src/app/contracts/create_product';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable, firstValueFrom, lastValueFrom } from 'rxjs';
import { List_Product } from 'src/app/contracts/list_product';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  constructor(private httpClientService: HttpClientService) {}

  create(
    product: Create_Product,
    successCallback?: () => void,
    errorCallback?: (errorMessage: Array<string>) => void
  ) {
    this.httpClientService
      .post(
        {
          controller: 'products',
        },
        product
      )
      .subscribe({
        next: (v) => console.log(v),
        error: (e: HttpErrorResponse) => {
          const _errors = e.error as Array<{
            key: string;
            value: Array<string>;
          }>;

          const _errorMessages = new Array<string>();

          _errors.forEach((error) => {
            error.value.forEach((value) => {
              _errorMessages.push(value);
            });
          });

          errorCallback?.(_errorMessages);
        },
        complete: () => successCallback?.(),
      });
  }

  async fetch(
    page: number = 0,
    size: number = 5,
    successCallback?: () => void,
    errorCallback?: (errorMessage: string) => void
  ): Promise<{ totalCount: number; products: List_Product[] }> {
    const promiseData: { totalCount: number; products: List_Product[] } =
      await lastValueFrom(
        this.httpClientService.get<{
          totalCount: number;
          products: List_Product[];
        }>({
          controller: 'products',
          queryString: `page=${page}&size=${size}`,
        })
      )
        .then((d) => {
          successCallback?.();
          return { totalCount: d.totalCount, products: d.products };
        })
        .catch((errorMessage: HttpErrorResponse) => {
          errorCallback?.(errorMessage.error);
          return { totalCount: 0, products: [] };
        });

    return promiseData;
  }

  async delete(id: string) {
    const deleteObservale: Observable<any> = this.httpClientService.delete<any>(
      {
        controller: 'products',
      },
      id
    );
    await firstValueFrom(deleteObservale);
  }
}
