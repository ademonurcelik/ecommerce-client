import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class HttpClientService {
  constructor(
    private httpClient: HttpClient,
    @Inject('baseUrl') private baseUrl: string
  ) {}

  private generateUrl(requestParams: Partial<RequestParameters>): string {
    return (
      `${requestParams.baseUrl ? requestParams.baseUrl : this.baseUrl}` +
      `/${requestParams.controller}` +
      `${requestParams.action ? `/${requestParams.action}` : ''}`
    );
  }

  get<T>(
    requestParams: Partial<RequestParameters>,
    id?: string
  ): Observable<T> {
    let url: string = '';

    if (requestParams.fullEndpoint) {
      url = requestParams.fullEndpoint;
    } else {
      url = `${this.generateUrl(requestParams)}${id ? `/${id}` : ''}${
        requestParams.queryString ? `?${requestParams.queryString}` : ``
      }`;
    }

    return this.httpClient.get<T>(url, { headers: requestParams.headers });
  }

  post<T>(
    requestParams: Partial<RequestParameters>,
    body: Partial<T>
  ): Observable<T> {
    let url: string = '';
    if (requestParams.fullEndpoint) {
      url = requestParams.fullEndpoint;
    } else {
      url = `${this.generateUrl(requestParams)}${
        requestParams.queryString ? `?${requestParams.queryString}` : ``
      }`;
    }

    return this.httpClient.post<T>(url, body, {
      headers: requestParams.headers,
    });
  }

  put<T>(
    requestParams: Partial<RequestParameters>,
    body: Partial<T>
  ): Observable<T> {
    let url: string = '';

    if (requestParams.fullEndpoint) {
      url = requestParams.fullEndpoint;
    } else {
      url = `${this.generateUrl(requestParams)}${
        requestParams.queryString ? `?${requestParams.queryString}` : ``
      }`;
    }

    return this.httpClient.put<T>(url, body, {
      headers: requestParams.headers,
    });
  }

  delete<T>(
    requestParams: Partial<RequestParameters>,
    id: string
  ): Observable<T> {
    let url: string = '';

    if (requestParams.fullEndpoint) {
      url = requestParams.fullEndpoint;
    } else {
      url = `${this.generateUrl(requestParams)}/${id}${
        requestParams.queryString ? `?${requestParams.queryString}` : ``
      }`;
    }

    return this.httpClient.delete<T>(url, { headers: requestParams.headers });
  }
}

export class RequestParameters {
  controller?: string;
  action?: string;
  queryString?: string;

  headers?: HttpHeaders;
  baseUrl?: string;
  fullEndpoint?: string;
}
