import { Component, OnInit } from '@angular/core';
import {
  AlertifyPosition,
  AlertifyService,
  AlertifyMessageType,
} from 'src/app/services/admin/alertify.service';
declare var alertify: any;

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css'],
})
export class LayoutComponent implements OnInit {
  constructor(private alertifyService: AlertifyService) {}

  ngOnInit(): void {}
}
