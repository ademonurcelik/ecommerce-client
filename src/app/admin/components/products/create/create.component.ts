import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { BaseComponent, SpinnerType } from 'src/app/base/base.component';
import { Create_Product } from 'src/app/contracts/create_product';
import {
  AlertifyMessageType,
  AlertifyPosition,
  AlertifyService,
} from 'src/app/services/admin/alertify.service';
import { ProductService } from 'src/app/services/common/models/product.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css'],
})
export class CreateComponent extends BaseComponent implements OnInit {
  constructor(
    spinnerService: NgxSpinnerService,
    private productService: ProductService,
    private alertifyService: AlertifyService
  ) {
    super(spinnerService);
  }

  ngOnInit(): void {}

  @Output() createdProduct: EventEmitter<Create_Product> = new EventEmitter()

  create(
    name: HTMLInputElement,
    description: HTMLInputElement,
    quantity: HTMLInputElement,
    price: HTMLInputElement
  ) {
    this.showSpinner(SpinnerType.BallClipRotatePulse);
    const createProduct: Create_Product = new Create_Product(
      name.value,
      description.value,
      parseInt(quantity.value),
      parseFloat(price.value)
    );

    this.productService.create(
      createProduct,
      () => {
        this.hideSpinner(SpinnerType.BallClipRotatePulse);
        this.alertifyService.message('Product created successfully', {
          delay: 5,
          messageType: AlertifyMessageType.Success,
          dismissOthers: true,
          position: AlertifyPosition.BottomRight,
        });
        this.createdProduct.emit(createProduct);
      },
      (errorMessage: Array<string>) => {
        this.alertifyService.dismissAll();
        this.hideSpinner(SpinnerType.BallClipRotatePulse);
        errorMessage.forEach((e) => {
          this.alertifyService.message(e, {
            delay: 5,
            messageType: AlertifyMessageType.Error,
            position: AlertifyPosition.BottomRight,
          });
        });
      }
    );
  }
}
