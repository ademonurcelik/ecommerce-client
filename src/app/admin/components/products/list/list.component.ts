import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { NgxSpinnerService } from 'ngx-spinner';
import { BaseComponent, SpinnerType } from 'src/app/base/base.component';
import { List_Product } from 'src/app/contracts/list_product';
import {
  AlertifyMessageType,
  AlertifyPosition,
  AlertifyService,
} from 'src/app/services/admin/alertify.service';
import { ProductService } from 'src/app/services/common/models/product.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
})
export class ListComponent extends BaseComponent implements OnInit {
  constructor(
    spinner: NgxSpinnerService,
    private productService: ProductService,
    private alertifyService: AlertifyService
  ) {
    super(spinner);
  }

  displayedColumns = [
    'name',
    'price',
    'stock',
    'created',
    'updated',
    'edit',
    'delete',
  ];
  dataSource: MatTableDataSource<List_Product> = null!;
  @ViewChild(MatPaginator) paginator: MatPaginator = null!;

  async getProducts() {
    this.showSpinner(SpinnerType.BallClipRotatePulse);

    const allProducts: { totalCount: number; products: List_Product[] } =
      await this.productService.fetch(
        this.paginator ? this.paginator.pageIndex : 0,
        this.paginator ? this.paginator.pageSize : 5,
        () => this.hideSpinner(SpinnerType.BallClipRotatePulse),
        (errorMessage) => {
          this.alertifyService.message(errorMessage, {
            delay: 5,
            dismissOthers: true,
            messageType: AlertifyMessageType.Error,
            position: AlertifyPosition.BottomRight,
          });
        }
      );
    this.dataSource = new MatTableDataSource<List_Product>(
      allProducts.products
    );
    this.paginator.length = allProducts.totalCount;
  }

  async handleChange() {
    await this.getProducts();
  }

  async handleDelete() {
    await this.getProducts();
  }

  async ngOnInit() {
    await this.getProducts();
  }
}
