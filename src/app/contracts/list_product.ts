export class List_Product {
  id: string;
  name: string;
  price: number;
  stock: number;
  createdAt: Date;
  updatedAt: Date;

  constructor(
    id: string,
    name: string,
    price: number,
    stock: number,
    createdAt: Date,
    updatedAt: Date
  ) {
    this.id = id;
    this.name = name;
    this.price = price;
    this.stock = stock;
    this.updatedAt = updatedAt;
    this.createdAt = createdAt;
  }
}
