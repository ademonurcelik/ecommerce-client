import {
  Directive,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  Output,
  Renderer2,
} from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { delay } from 'rxjs';
import { SpinnerType } from 'src/app/base/base.component';
import {
  AlertifyMessageType,
  AlertifyPosition,
  AlertifyService,
} from 'src/app/services/admin/alertify.service';
import { ProductService } from 'src/app/services/common/models/product.service';

declare var $: any;

@Directive({
  selector: '[appDelete]',
})
export class DeleteDirective {
  constructor(
    private element: ElementRef,
    private _renderer: Renderer2,
    private productService: ProductService,
    private alerfiyService: AlertifyService,
    private spinner: NgxSpinnerService
  ) {
    const img = _renderer.createElement('img');
    img.setAttribute('src', '../../../../../assets/trash.circle.fill.svg');
    img.setAttribute('style', 'cursor: pointer');
    _renderer.appendChild(element.nativeElement, img);
  }

  @Input() id: string = '';
  @Input() name: string = '';
  @Output() handleDelete: EventEmitter<any> = new EventEmitter();

  @HostListener('click')
  async onClick() {
    this.spinner.show(SpinnerType.LineSpinClockwiseFade);
    const td: HTMLTableCellElement = this.element.nativeElement;
    await this.productService.delete(this.id);
    this.alerfiyService.message(`Product ${this.name} deleted successfully`, {
      delay: 5,
      dismissOthers: true,
      messageType: AlertifyMessageType.Success,
      position: AlertifyPosition.BottomRight,
    });
    $(td.parentElement).fadeOut(500, () => {
      this.handleDelete.emit();
      this.spinner.hide(SpinnerType.LineSpinClockwiseFade);
    });
  }
}
